package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.Service;
import sample.helpers.CustomFont;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;

public class AdminServiceCell extends ListCell<Service> {
    @FXML
    private ImageView image;

    @FXML
    private Label serviceNameLabel;

    @FXML
    private Label newCost;


    @FXML
    private Label oldCost;

    @FXML
    private Label duration;

    @FXML
    private Label discount;

    @FXML
    private HBox hBox;

    @FXML
    private Button editBtn;

    @FXML
    private Button deleteBtn;

    private FXMLLoader mLLoader;

    @Override
    protected void updateItem(Service service, boolean empty) {
        super.updateItem(service, empty);

        BaseData baseData = BaseData.getBaseData();


        if (empty || service == null) {

            setText(null);
            setGraphic(null);

        } else {
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("\\admin_service_cell.fxml"));
                mLLoader.setController(this);

                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            hBox.setBackground(new Background(new BackgroundFill(
                    Color.rgb(231, 250, 191),
                    CornerRadii.EMPTY,
                    Insets.EMPTY)));
            serviceNameLabel.setFont(CustomFont.getFont());
            deleteBtn.setFont(CustomFont.getFont());
            deleteBtn.setStyle("-fx-background-color: #04a0ff; -fx-text-fill: white; -fx-font-size: 9");
            editBtn.setStyle("-fx-background-color: #04a0ff; -fx-text-fill: white; -fx-font-size: 9");
            editBtn.setFont(CustomFont.getFont());
            newCost.setFont(CustomFont.getFont());
            oldCost.setFont(CustomFont.getFont());
            discount.setFont(CustomFont.getFont());
            duration.setFont(CustomFont.getFont());
            File file = new File("src/sample/" + service.mainImagePath);
            String localUrl = null;
            try {
                localUrl = file.toURI().toURL().toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            Image image1 = new Image(localUrl);
            image.setImage(image1);
            serviceNameLabel.setText(service.title);
            oldCost.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

            if (service.discount > 0) {
                oldCost.setText(Integer.toString((int) service.getOldCost()));
                newCost.setText((int) service.cost + " рублей");
                duration.setText(" за " + service.duration + " мин.");
                discount.setText("* скидка " + service.discount + "%");
            } else {
                newCost.setText((int) service.cost + " рублей");
                duration.setText(" за " + service.duration + " мин.");
                discount.setText("");
            }

            deleteBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    try {
                        if (baseData.selectRecords("*", " WHERE ServiceID = " + service.id).size() > 0) {
                            Stage stage = new Stage();
                            Parent root = FXMLLoader.load(getClass().getResource("danger_window.fxml"));
                            Scene scene = new Scene(root, 200, 150);
                            stage.setScene(scene);
                            stage.show();
                        } else {
                            hBox.setDisable(true);
                            deleteBtn.setDisable(true);
                            editBtn.setDisable(true);
                            baseData.deleteService("id=" + service.id);
                        }

                    } catch (SQLException | IOException exception) {
                        exception.printStackTrace();
                    }
                }
            });

            editBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
        Stage stage = new Stage();
                    try {

                        FXMLLoader loader = new FXMLLoader(getClass().getResource("edit_service.fxml"));
                        Parent root = loader.load();
                        Scene scene = new Scene(root,400,350);
                        stage.setScene(scene);
                        EditService editService =loader.getController();
                        editService.id = service.id;
                        editService.fillData(service.title,Double.toString(service.cost),
                                Integer.toString(service.duration),service.description,
                                Integer.toString(service.discount));
                        stage.show();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            });
            setText(null);
            setGraphic(hBox);
        }

    }

}
