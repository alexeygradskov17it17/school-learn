package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class Main extends Application {
    Timer timer = new Timer();
    String combination = "";
    public boolean isAdministratorMode;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("res/sample.fxml"));
        Parent root2 = FXMLLoader.load(getClass().getResource("res/admin_window_list_of_services.fxml"));
        File file = new File("src/sample/school_logo.png");
        String localUrl = file.toURI().toURL().toString();
        Image image = new Image(localUrl);
        primaryStage.getIcons().add(image);
        primaryStage.setTitle("Школа языков “Леарн”");
        Scene scene = new Scene(root, 700, 500);
        Scene adminScene = new Scene(root2,700,500);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.DIGIT0) {
                    if (timer != null) {
                        timer.cancel();
                        timer= new Timer();
                    }
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            clearCombination();
                        }
                    }, 1000);

                    combination = combination + "0";
                    if (combination.equals("000000")) {
                        primaryStage.close();
                        primaryStage.setScene(adminScene);
                        primaryStage.setTitle("Школа языков “Леарн”");
                        primaryStage.show();
                    }
                }
            }
        });
    }

    public void clearCombination() {
        combination = "";
    }


    public static void main(String[] args) {
        launch(args);
    }
}
