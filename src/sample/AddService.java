package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.SQLException;

public class AddService {
    public Label titleLabel;
    public Label costLabel;
    public Label labelDuration;
    public Label labelDiscount;
    public Label labelDescription;
    public TextField title;
    public TextField cost;
    public TextField duration;
    public TextField discount;
    public TextField description;
    public Button saveBtn;
    BaseData baseData = BaseData.getBaseData();

    public void save(ActionEvent actionEvent) throws SQLException {
        Service service = new Service(0,"",0,0,"",0,"");
        if (!title.getText().equals("")&&
                !cost.getText().equals("")&&
                !duration.getText().equals("")&&
                !discount.getText().equals("")&&
                !description.getText().equals("")){
            service.title = title.getText();
            service.cost = Double.parseDouble(cost.getText());
            service.duration = Integer.parseInt(duration.getText());
            service.description = description.getText();
            service.discount = Integer.parseInt(discount.getText());
            baseData.insertService(service);
            Stage stage = (Stage) description.getScene().getWindow();
            stage.close();
        }
    }
}
