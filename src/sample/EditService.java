package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.helpers.CustomFont;

import java.sql.SQLException;

public class EditService {
    public TextField title;
    public TextField discount;
    public TextField duration;
    public TextField cost;
    public TextField description;
    public Button saveBtn;
    public Label titleLabel;
    public Label costLabel;
    public Label labelDiscount;
    public Label labelDuration;
    public Label labelDescription;
    BaseData baseData = BaseData.getBaseData();
    int id;

    public void initialize() {
        saveBtn.setBackground(new Background(new BackgroundFill(
                Color.rgb(4, 160, 255),
                CornerRadii.EMPTY,
                Insets.EMPTY)));
        title.setFont(CustomFont.getFont());
        discount.setFont(CustomFont.getFont());
        duration.setFont(CustomFont.getFont());
        cost.setFont(CustomFont.getFont());
        description.setFont(CustomFont.getFont());
        saveBtn.setFont(CustomFont.getFont());
        titleLabel.setFont(CustomFont.getFont());
        labelDescription.setFont(CustomFont.getFont());
        labelDiscount.setFont(CustomFont.getFont());
        labelDuration.setFont(CustomFont.getFont());
        costLabel.setFont(CustomFont.getFont());
    }

    public void fillData(String titleTxt, String costTxt,
                         String durationTxt, String descriptionTxt,
                         String discountTxt) {
        title.setText(titleTxt);
        cost.setText(costTxt);
        duration.setText(durationTxt);
        description.setText(descriptionTxt);
        discount.setText(discountTxt);
    }

    public void save(ActionEvent actionEvent) throws SQLException {
        baseData.updateService(new Service(id, title.getText(), Double.parseDouble(cost.getText()), Integer.parseInt(duration.getText()), description.getText(), Integer.parseInt(discount.getText()), " "));
        Stage stage = (Stage) title.getScene().getWindow();
        stage.close();
    }
}
