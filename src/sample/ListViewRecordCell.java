package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import sample.helpers.CustomFont;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;

public class ListViewRecordCell extends ListCell<Record> {
    @FXML
    private Text textServiceTitle;

    @FXML
    private Text textDateTime;

    @FXML
    private Text clientName;

    @FXML
    private HBox hBox;


    private FXMLLoader mLLoader;

    @Override
    protected void updateItem(Record record, boolean empty) {
        super.updateItem(record, empty);


            if (empty || record == null) {

                setText(null);
                setGraphic(null);

            } else {
                if (mLLoader == null) {
                    mLLoader = new FXMLLoader(getClass().getResource("\\record_cell.fxml"));
                    mLLoader.setController(this);

                    try {
                        mLLoader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                BaseData baseData = BaseData.getBaseData();
                try {
                Client client = baseData.selectClient("*"," id="+record.clientID);
                    textServiceTitle.setText(baseData.selectService("*"," id="+record.serviceID).title);
                    clientName.setText(client.firstName+" "+client.lastName+" "+client.patronymic);
                    textDateTime.setText(record.datetime);
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
                hBox.setBackground(new Background(new BackgroundFill(Color.rgb(231, 250, 191),
                        CornerRadii.EMPTY,
                        Insets.EMPTY)));


                setText(null);
                setGraphic(hBox);
            }

        }
}
