package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import sample.helpers.CustomFont;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

public class ListViewServiceCell extends ListCell<Service> {
    @FXML
    private ImageView image;

    @FXML
    private Label serviceNameLabel;

    @FXML
    private Label newCost;

    @FXML
    private Label oldCost;

    @FXML
    private Label duration;

    @FXML
    private Label discount;

    @FXML
    private HBox hBox;


    private FXMLLoader mLLoader;

    @Override
    protected void updateItem(Service service, boolean empty) {
        super.updateItem(service, empty);


        if (empty || service == null) {

            setText(null);
            setGraphic(null);

        } else {
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("\\service_cell.fxml"));
                mLLoader.setController(this);

                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            hBox.setBackground(new Background(new BackgroundFill(
                    Color.rgb(231, 250, 191),
                    CornerRadii.EMPTY,
                    Insets.EMPTY)));
            serviceNameLabel.setFont(CustomFont.getFont());
            newCost.setFont(CustomFont.getFont());
            oldCost.setFont(CustomFont.getFont());
            discount.setFont(CustomFont.getFont());
            duration.setFont(CustomFont.getFont());
            File file = new File("src/sample/" + service.mainImagePath);
            String localUrl = null;
            try {
                localUrl = file.toURI().toURL().toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            Image image1 = new Image(localUrl);
            image.setImage(image1);
            serviceNameLabel.setText(service.title);
            oldCost.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

            if (service.discount > 0) {
                oldCost.setText(Integer.toString((int) service.getOldCost()));
                newCost.setText((int) service.cost + " рублей");
                duration.setText(" за " + service.duration + " мин.");
                discount.setText("* скидка " + service.discount + "%");
            } else {
                newCost.setText((int) service.cost + " рублей");
                duration.setText(" за " + service.duration + " мин.");
                discount.setText("");
            }


            setText(null);
            setGraphic(hBox);
        }

    }
}

