package sample;

import com.sun.deploy.util.FXLoader;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.stage.Stage;
import sample.helpers.CustomFont;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;

public class ListRecords {
    public ListView<Record> lvRecords;
    public Button btnAddRecord;
    BaseData baseData = BaseData.getBaseData();
    ObservableList<Record> recordsObservableList;
    Timer timer = new Timer();

    public void initialize(){
        btnAddRecord.setFont(CustomFont.getFont());
        try {
            recordsObservableList = FXCollections.observableArrayList(baseData.selectRecords("*",""));
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        lvRecords.setItems(recordsObservableList);
        lvRecords.setCellFactory(studentListView -> new ListViewRecordCell());
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    public void run() {
                        initialize();
                    }
                });
            }
        }, 30000);
    }

    public void addRecord(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("add_record.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root,400,350);
        stage.setScene(scene);
        stage.show();
    }
}
