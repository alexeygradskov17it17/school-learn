package sample;

public class Record {
    public int id, clientID, serviceID;
    public String datetime, comment;

    public Record(int id, int clientID, int serviceID,
            String datetime, String comment){
        this.id=id;
        this.clientID=clientID;
        this.serviceID =serviceID;
        this.datetime=datetime;
        this.comment=comment;
    }
}
