package sample.helpers;

import javafx.scene.text.Font;


public class CustomFont {
    static Font font;
    static String fontPath = "file:src/sample/fonts/comic.ttf";
    static int size = 11;


     private CustomFont(){}

     public static Font getFont(){
         if (font==null){
             font = Font.loadFont(fontPath,size);
         }
         return font;
     }


}
