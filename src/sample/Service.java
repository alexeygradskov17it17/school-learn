package sample;

public class Service {
    public int id, duration,discount;
    public String title, description, mainImagePath;
    public double cost;

    public Service(int id, String title, double cost,
                   int duration, String description,
                   int discount, String mainImagePath) {
        this.id = id;
        this.title = title;
        this.cost = cost;
        this.duration = duration;
        this.description = description;
        this.discount = discount;
        this.mainImagePath = mainImagePath;
    }

    public double getOldCost(){
        return cost+((cost*discount)/100);
    }
}
