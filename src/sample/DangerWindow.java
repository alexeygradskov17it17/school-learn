package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class DangerWindow {
    public Button btnCloseDangerWindow;

    public void close_window(ActionEvent actionEvent) {
        Stage stage = (Stage)btnCloseDangerWindow.getScene().getWindow();
        stage.close();
    }
}
