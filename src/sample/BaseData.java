package sample;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class BaseData {
    static Connection connection;
    static Statement stmt = null;
    public static BaseData baseData;

    public static BaseData getBaseData() {
        if (baseData == null) {
            baseData = new BaseData();
        }
        return baseData;
    }

    private BaseData() {
        connect("jdbc:sqlserver://%1$s;databaseName=%2$s;user=%3$s;password=%4$s;", "DESKTOP-T71AMPQ\\DATA",
                "school_date",
                "sa",
                "gradskov"
        );

    }


    public void connect(String connectionUrl, String instanceName, String databaseName, String userName, String pass) {
        try {
            String connectionString = String.format(connectionUrl, instanceName, databaseName, userName, pass);
            connection = DriverManager.getConnection(connectionString);
            stmt = connection.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(BaseData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public ArrayList<Service> select(String columnNames, String[] predicates, String orderBy) throws SQLException {
        predicates[0] = predicates[0];
        ArrayList<Service> services = new ArrayList<>();
        ResultSet rs;
        if (predicates[0].equals("discount=discount") && predicates[1].equals("Title=Title")) {
            rs = stmt.executeQuery("SELECT " + columnNames + " FROM Service " + orderBy);
        } else
            rs = stmt.executeQuery("SELECT " + columnNames + " FROM Service WHERE " + predicates[0] + " AND " + predicates[1] + orderBy);
        while (rs.next()) {
            services.add(new Service(rs.getInt(1), rs.getString(2),
                    rs.getDouble(3), rs.getInt(4),
                    rs.getString(5), rs.getInt(6),
                    rs.getString(7)));
        }
        return services;
    }

    public Client selectClient(String columns, String predicate) throws SQLException {
        ResultSet resultSet;
        if (predicate.equals("")) {
            resultSet = stmt.executeQuery("SELECT " + columns + " FROM Client");
        } else resultSet = stmt.executeQuery("SELECT " + columns + " FROM Client WHERE " + predicate);
        resultSet.next();
        return new Client(resultSet.getInt(1), resultSet.getString(2),
                resultSet.getString(3), resultSet.getString(4),
                resultSet.getString(5), resultSet.getString(6),
                resultSet.getString(7), resultSet.getString(8),
                resultSet.getInt(9), resultSet.getString(10));
    }

    public Service selectService(String columns, String predicate) throws SQLException {
        ResultSet resultSet;
        if (predicate.equals("")) {
            resultSet = stmt.executeQuery("SELECT " + columns + " FROM Service");
        } else resultSet = stmt.executeQuery("SELECT " + columns + " FROM Service WHERE " + predicate);
        resultSet.next();
        return new Service(resultSet.getInt(1), resultSet.getString(2),
                resultSet.getDouble(3), resultSet.getInt(4),
                resultSet.getString(5), resultSet.getInt(6),
                resultSet.getString(7));
    }

    public ArrayList<Record> selectRecords(String columnNames, String predicate) throws SQLException {
        ArrayList<Record> records = new ArrayList<>();
        ResultSet rs;
        rs = stmt.executeQuery("SELECT " + columnNames + " FROM ClientService " + predicate + " ORDER BY time ASC");
        while (rs.next()) {
            records.add(
                    new Record(rs.getInt(1),
                            rs.getInt(2), rs.getInt(3),
                            rs.getString(4), rs.getString(5)));
        }
        return records;
    }

    public void deleteService(String predicate) throws SQLException {
        stmt.execute("DELETE FROM Service WHERE " + predicate);
    }

    public void updateService(Service service) throws SQLException {
        stmt.executeUpdate("UPDATE Service SET title = '" + service.title +
                "',cost=" + service.cost + ",DurationInSeconds = " + service.duration +
                ",Description = '" + service.description + "',Discount=" + service.discount + " WHERE id=" + service.id);
    }

    public void insertRecord(Record record) throws SQLException {
        stmt.execute("INSERT INTO ClientService VALUES(" + record.clientID + "," +
                record.serviceID + ",'" + record.datetime + "','" + record.comment + "')");
    }

    public ArrayList<Client> selectClients(String columnNames, String predicate) throws SQLException {
        ArrayList<Client> clients = new ArrayList<>();
        ResultSet rs;
        rs = stmt.executeQuery("SELECT " + columnNames + " FROM Client " + predicate);
        while (rs.next()) {
            clients.add(
                    new Client(rs.getInt(1), rs.getString(2),
                            rs.getString(3), rs.getString(4),
                            rs.getString(5), rs.getString(6),
                            rs.getString(7), rs.getString(8),
                            rs.getInt(9), rs.getString(10)));
        }
        return clients;
    }

    public void insertService(Service service) throws SQLException {
        stmt.execute("INSERT INTO Service VALUES('"
                +service.title+"',"
                +service.cost+","
                +service.duration+",'"
                +service.description+"',"
                +service.discount+",'')");
    }
}