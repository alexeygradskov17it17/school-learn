package sample;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.AdminServiceCell;
import sample.BaseData;
import sample.ListViewServiceCell;
import sample.Service;
import sample.helpers.CustomFont;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class AdminWindowListOfServices {
    public ChoiceBox<String> sizesOfDiscount;
    public ListView<Service> listView;
    public Button btnOpenListOfRecords;
    public TextField findField;
    public ChoiceBox<String> orderByCB;
    public Button addServiceBtn;
    List<Service> services;
    String[] predicates = {"discount=discount", "Title=Title"};
    BaseData baseData = BaseData.getBaseData();
    String value = "все";
    ObservableList<String> discounts = FXCollections.observableArrayList(
            "все",
            "от 0 до 5%",
            "от 5% до 15%",
            "от 15% до 30%",
            "от 30% до 70%",
            "от 70% до 100%");
    Timer timer = new Timer(), timer2 = new Timer();
    ObservableList<String> strings = FXCollections.observableArrayList("по цене(низ)", "по цене(выс)");
    String orderBy = "";

    public void initialize() throws SQLException {
        btnOpenListOfRecords.setFont(CustomFont.getFont());
        findField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (timer != null) {
                timer.cancel();
                timer = new Timer();
            }
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(new Runnable() {
                        public void run() {
                            find(newValue);
                        }
                    });

                }
            }, 500);
        });

        addServiceBtn.setFont(CustomFont.getFont());
        addServiceBtn.setBackground(new Background(new BackgroundFill(Color.rgb(4, 160, 255), CornerRadii.EMPTY, Insets.EMPTY)));

        timer2.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    public void run() {
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                    }
                });
            }
        }, 30000);

        sizesOfDiscount.setValue(value);
        services = baseData.select("*", predicates, orderBy);
        ObservableList<Service> servicesObservableList = FXCollections.observableArrayList(services);
        listView.setItems(servicesObservableList);
        listView.setCellFactory(servicesListView -> new AdminServiceCell());
        orderByCB.setItems(strings);
        orderByCB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                orderByCB.setValue(orderByCB.getValue());
                switch (orderByCB.getValue()) {
                    case "по цене(низ)":
                        orderBy = " ORDER BY Cost ASC";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "по цене(выс)":
                        orderBy = " ORDER BY Cost DESC";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                }
            }
        });
        sizesOfDiscount.setItems(discounts);
        sizesOfDiscount.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                sizesOfDiscount.setValue(sizesOfDiscount.getValue());
                switch (sizesOfDiscount.getValue()) {
                    case "все":
                        predicates[0] = "discount=discount";
                        value = "все";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 0 до 5%":
                        predicates[0] = "discount>=0 AND discount<5";
                        value = "от 0 до 5%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 5% до 15%":
                        predicates[0] = "discount>=5 AND discount<15";
                        value = "от 5% до 15%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 15% до 30%":
                        predicates[0] = " discount>=15 AND discount<30";
                        value = "от 15% до 30%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 30% до 70%":
                        predicates[0] = "discount>=30 AND discount<70";
                        value = "от 30% до 70%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                    case "от 70% до 100%":
                        predicates[0] = "discount>=70 AND discount<100";
                        value = "от 70% до 100%";
                        try {
                            initialize();
                        } catch (SQLException exception) {
                            exception.printStackTrace();
                        }
                        break;
                }
            }
        });

        btnOpenListOfRecords.setFont(CustomFont.getFont());
        btnOpenListOfRecords.setBackground(new Background(new BackgroundFill(
                Color.rgb(4, 160, 255),
                CornerRadii.EMPTY,
                Insets.EMPTY)));
    }

    public void find(String newValue) {
        if (!newValue.equals("".trim())) {
            predicates[1] = "Title LIKE '" + newValue.toLowerCase().trim() + "%'";
        } else predicates[1] = "Title=Title";
        try {
            initialize();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void openWindowWithRecords(ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("\\list_records.fxml"));
        Scene scene = new Scene(root, 700, 500);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public void addService(ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("add_service.fxml"));
        Scene scene = new Scene(root, 400, 350);
        stage.setScene(scene);
        stage.show();
    }
}
